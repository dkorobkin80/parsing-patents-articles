#Программа предназначена для парсинга русскоязычных журнальных статей
#Извлечение элементов описания англоязычных статей производится со страницы https://rajpub.com/index.php/jap/issue/view/741
#Извлечение элементов описания русскоязычных статей производится со страницы http://journals.ioffe.ru/issues/1993

#Импорт библиотек
#pip install requests - для работы с запросами
#pip install BeautifulSoup - для парсинга документов
#pip install csv - для записи извлеченных данных в файл формата csv
#pip install pymongo - для работы с БД

import requests, sys
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient

#Подключение к СУБД MongoDB
client = MongoClient()
#Создание базы данных
db = client.singledatabase1
#Создание коллекции в созданной на предыдущем шаге БД
articlees = db.articlees

HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.0.905 Yowser/2.5 Safari/537.36'}
#Запрос на получение содержимого страницы
def get_html(url):
    rrequests = requests.get(url, headers=HEADERS)
    if rrequests.status_code != requests.codes.ok:
        print('Ошибка при загрузке страницы! ')
        sys.exit()
    return rrequests.text

#Блок для получения всех внутренних ссылок с искомой страницы
def get_all_links(html):
    #Обработка содержимого посредством BeautifulSoup, с использованием синтаксического анализатора lxml
    soup = BeautifulSoup(html, 'lxml')

    divs = soup.select("div.issue_art_title")
    links = []
    #Поиск всех ссылок на статьи
    for div in divs:
        a = div.find('a').get('href')
        link = 'http://journals.ioffe.ru'+ a
        # Запись найденных ссылок в массив
        links.append(link)

    return links

#Блок извлечения элементов из тегов и запись их в БД
def get_page_data(html):

    soup = BeautifulSoup(html,'lxml')
    #Обработка исключений в зависимости от искходной ссылки
    try:
        title = soup.find('div', class_='art_title').text.strip()
    except:
        title = ''
    try:
        authors = soup.find('div', class_='authors').text.strip()
    except:
        authors = ''
    try:
        organizations = soup.find('div', class_='organizations').text.strip()
    except:
        organizations = ''
    try:
        date = soup.find('div', class_='art_date_online').text.strip()
    except:
        date = ''
    try:
        abstract = soup.find('div', id='yw4_tab_0').text.strip()
    except:
        abstract = ''
    try:
        pdf = soup.find('div', class_='art_pdf').find('a').get('href')
        url_pdf = 'http://journals.ioffe.ru' + pdf

    except:
        url_pdf = ''

    #Формирование словаря (коллекции) для записи в БД
    data = {'title': title,
            'author': authors,
            'organizations': organizations,
            'date': date,
            'abstract': abstract,
            'url_pdf': url_pdf}

    #Запись в БД
    articlees.insert_one(data)
    return data

#Функция для записи извлеченных данных в файл формата csv
def write_csv(data):
    with open('dataartiсle.csv', 'a', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow((data['title'],
                         data['author'],
                         data['organizations'],
                         data['date'],
                         data['abstract'],
                         data['url_pdf']))
        print(data['title'], 'parsed')
        print(data['author'], 'parsed')
        print(data['organizations'], 'parsed')
        print(data['date'], 'parsed')
        print(data['abstract'], 'parsed')
        print(data['url_pdf'], 'parsed')

def main():
    url = 'http://journals.ioffe.ru/issues/2199'

    all_links = get_all_links(get_html(url))
    for index, url in enumerate(all_links):
        html = get_html(url)
        data = get_page_data(html)
        write_csv(data)
if __name__ == '__main__':
    main()
