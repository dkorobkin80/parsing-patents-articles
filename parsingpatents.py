# Программа для парсинга патентов USPTO и RUPTO
# Для извлечения элементов описания, необходимо указать директорию в переменной path
# С помощью данной программы производится обработка xml-документов
'''
Из найденного документа извлекаются элементы описания патентов: название, номер патента, дата публикации,
авторы, класс МПК, абстракт, заявка, описание и ссылка на документ, размещенный локально
'''''
# Для записи данных в БД MongoDB, необходимо запустить сервер

#pip install BeautifulSoup
#pip install pymongo
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient
import zipfile, fnmatch, os
import glob

#Подключение к СУБД MongoDB
client = MongoClient()
#Создание базы данных
db = client.singledatabase8
#Создание коллекции в созданной на предыдущем шаге БД
patents = db.patents

path = r"C:\pa"  # Директория, из которой будут обрабатываться документы


def parsingPatents(path):
    # Если в директории найдены архивы, необходимо распаковать их
    Zip = '*.zip'

    for root, dirs, files in os.walk(path):
        for filename in fnmatch.filter(files, Zip):
            print('Распаковка архива')
            print(os.path.join(root, filename))
            zipfile.ZipFile(os.path.join(root, filename)).extractall(os.path.join(root, os.path.splitext(filename)[0]))

    # Поиск документов с xml-расширением
    if os.path.isdir(path):
        files = glob.glob(path + r'\**\*.xml', recursive=True)

        for f in files:
            diru = (f)
            # Открытие найденного файла
            with open(diru, encoding='utf-8') as f:
                contents = f.read()
            # Обработка документов при помощи BeautifulSoup
            soup = BeautifulSoup(contents, 'lxml')
            try:
                number = soup.find('b110').text.strip()
            except:
                number = ''
            try:
                title = soup.find('ru-b542').text.strip()
            except:
                title = soup.find('b540').text.strip()
            try:
                date = soup.find('b140').text.strip()
            except:
                date = ''
            try:
                country = soup.find('b190').text.strip()
            except:
                country = ''
            try:
                authors = soup.find('fnm').text.strip()
            except:
                authors = soup.find('ru-name-text').text.strip()
            try:
                mpk = soup.find('b130').text.strip()
            except:
                mpk = ''
            try:
                abstract = soup.find('abstract').text.strip()
            except:
                abstract = soup.find('ptext').text.strip()
            try:
                claim = soup.find('brfsum').text.strip()
            except:
                claim = soup.find('claim-text').text.strip()
            try:
                description = soup.find('description').text.strip()  # soup.find('drwdesc').text.strip()
            except:
                description = soup.find('detdesc').text.strip()

            # Запись извлеченных элементов описания патентов в базу данных
            data = {'number': number,
                    'title': title,
                    'date': date,
                    'country': country,
                    'authors': authors,
                    'mpk': mpk,
                    'abstract': abstract,
                    'claim': claim,
                    'description': description,
                    'patents': diru}
            patents.insert_one(data)

            # Запись извлеченных данных в файл csv
            with open('datapatent.csv', 'a', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow((data['number'],
                                 data['title'],
                                 data['date'],
                                 data['country'],
                                 data['authors'],
                                 data['mpk'],
                                 data['abstract'],
                                 data['claim'],
                                 data['description']))
                print(data['number'], 'parsed')
                print(data['title'], 'parsed')
                print(data['date'], 'parsed')
                print(data['country'], 'parsed')
                print(data['authors'], 'parsed')
                print(data['mpk'], 'parsed')
                print(data['abstract'], 'parsed')
                print(data['claim'], 'parsed')
                print(data['description'], 'parsed')

    else:
        print('Такой диретории не существует, введите существующую:')


parsingPatents(path)
