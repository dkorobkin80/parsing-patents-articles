#Программа предназначена для парсинга диссертаций

#Импорт библиотек
#pip install requests - для работы с запросами
#pip install BeautifulSoup - для парсинга документов
#pip install csv - для записи извлеченных данных в файл формата csv
#pip install pymongo - для работы с БД

import requests, sys
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient

#Подключение к СУБД MongoDB
client = MongoClient()
#Создание базы данных
db = client.singledatabase
#Создание коллекции в созданной на предыдущем шаге БД
articlees = db.articlees

HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 YaBrowser/20.6.0.905 Yowser/2.5 Safari/537.36'}
#Запрос на получение содержимого страницы
def get_html(url):
    rrequests = requests.get(url, headers=HEADERS)
    if rrequests.status_code != requests.codes.ok:
        print('Ошибка при загрузке страницы! ')
        sys.exit()
    return rrequests.text

#Блок для получения всех внутренних ссылок с искомой страницы
def get_all_links(html):
    #Обработка содержимого посредством BeautifulSoup, с использованием синтаксического анализатора lxml
    soup = BeautifulSoup(html, 'lxml')

    links = []
    divs = soup.select("td.h4")
    #Поиск всех ссылок 
    for div in divs:
        a = div.find('a').get('href')
        link = 'https://www.dissercat.com'+ a
        # Запись найденных ссылок в массив
        links.append(link)
    return links

#Блок извлечения элементов из тегов и запись их в БД
def get_page_data(html):

    soup = BeautifulSoup(html,'lxml')
    #Обработка исключений в зависимости от искходной ссылки
    try:
        title = soup.find('b', itemprop='name').text.strip()
    except:
        title = ''
    try:
        authors = soup.find('span', itemprop='author').text.strip()
    except:
        authors = ''
    try:
        academic_degree = soup.find('span', itemprop='inSupportOf').text.strip()
    except:
        academic_degree = ''
    try:
        date = soup.find('span', itemprop='datePublished').text.strip()
    except:
        date = ''
    try:
        abstract = soup.find('div', class_='img-holder').find('a').get('href')
        abstract = 'https://www.dissercat.com' + abstract

    except:
        url_pdf = ''

    #Формирование словаря (коллекции) для записи в БД
    data = {'title': title,
            'author': authors,
            'academic_degree': academic_degree,
            'date': date,
            'abstract': abstract}

    #Запись в БД
    articlees.insert_one(data)
    return data

#Функция для записи извлеченных данных в файл формата csv
def write_csv(data):
    with open('datadissertation.csv', 'a', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow((data['title'],
                         data['author'],
                         data['academic_degree'],
                         data['date'],
                         data['abstract']))
        print(data['title'], 'parsed')
        print(data['author'], 'parsed')
        print(data['academic_degree'], 'parsed')
        print(data['date'], 'parsed')
        print(data['abstract'], 'parsed')

def main():
    url = 'https://www.dissercat.com/catalog/fiziko-matematicheskie-nauki/fizika/lazernaya-fizika'

    all_links = get_all_links(get_html(url))
    for index, url in enumerate(all_links):
        html = get_html(url)
        data = get_page_data(html)
        write_csv(data)
if __name__ == '__main__':
    main()
