# Программа предназначена для парсинга англоязычных журнальных статей
# Извлечение элементов описания англоязычных статей производится со страницы https://rajpub.com/index.php/jap/issue/view/741
# Извлечение элементов описания русскоязычных статей производится со страницы http://journals.ioffe.ru/issues/1993

'''
Производится извлечение элементов описания статей: : название, компания, авторы, дата публикации, аннотация, ссылка на PDF-файл документа;
'''''

# Импорт библиотек
# pip install requests - для работы с запросами
# pip install BeautifulSoup - для парсинга документов
# pip install csv - для записи извлеченных данных в файл формата csv
# pip install pymongo - для работы с БД

import requests, sys
from bs4 import BeautifulSoup
import csv
from pymongo import MongoClient

# Подключение к СУБД MongoDB
client = MongoClient()
# Создание базы данных
db = client.singledatabas7
# Создание коллекции в созданной на предыдущем шаге БД
articlees = db.articlees

HEADERS = {'user-agent':
               'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
               'Chrome/81.0.4044.138 YaBrowser/20.6.0.905 Yowser/2.5 Safari/537.36'}


# Запрос на получение содержимого страницы
def get_html(url):
    r = requests.get(url, headers=HEADERS)
    if r.status_code != requests.codes.ok:
        print('Ошибка при загрузке страницы! ')
        sys.exit()
    return r.text


# Блок для получения всех внутренних ссылок с искомой страницы
def get_all_links(html):
    soup = BeautifulSoup(html, 'lxml')
    # Условия для выполнения парсинга англоязычных журнальных статей

    divs = soup.select("h3.title")  # soup.find('div', class_='span9').find_all('div', class_='issue_art_title')
    links = []

    # Поиск всех ссылок на статьи
    for div in divs:
        link = div.find('a').get('href')
        # Запись найденных ссылок в массив
        links.append(link)
    return links


# Блок извлечения элементов из тегов и запись их в БД
def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    # Обработка исключений в зависимости от искходной ссылки
    try:
        title = soup.find('h1', class_='page_title').text.strip()

    except: title = ''
    try:
        authors = soup.find('ul', class_='authors').find('li').find('span', class_='name').text.strip()
    except:
        authors = ''
    try:
        organizations = soup.find('span', class_='affiliation').text.strip()
    except:
        organizations = ''
    try:
        date = soup.find('div', class_='item published').find('section', class_='sub_item').find('div', class_='value').find(
        'span').text.strip()
    except:
        date = ''
    try:
        abstract = soup.find('section', class_='item abstract').find('p').text.strip()
    except:
        abstract = ''
    try:
        url_pdf = soup.find('ul', class_='value galleys_links').find('li').find('a').get('href')

    except:
        url_pdf = ''

# Формирование словаря (коллекции) для записи в БД
    data = {'title': title,
            'author': authors,
            'organizations': organizations,
            'date': date,
            'abstract': abstract,
            'url_pdf': url_pdf}

    # Запись в БД
    articlees.insert_one(data)
    return data


# Функция для записи извлеченных данных в файл формата csv
def write_csv(data):
    with open('dataartiсle.csv', 'a', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow((data['title'],
                         data['author'],
                         data['organizations'],
                         data['date'],
                         data['abstract'],
                         data['url_pdf']))
        print(data['title'], 'parsed')
        print(data['author'], 'parsed')
        print(data['organizations'], 'parsed')
        print(data['date'], 'parsed')
        print(data['abstract'], 'parsed')
        print(data['url_pdf'], 'parsed')


def main():
    url = 'https://rajpub.com/index.php/jap/issue/view/776'
    all_links = get_all_links(get_html(url))
    for index, url in enumerate(all_links):
        html = get_html(url)
        data = get_page_data(html)
        write_csv(data)
        print(index)


if __name__ == '__main__':
    main()


